//
//  ViewController.swift
//  facebook_demo_asghar
//
//  Created by user on 01/02/18.
//  Copyright © 2018 user. All rights reserved.
//

import UIKit

class ViewController: UIViewController, FBSDKLoginButtonDelegate {
    
    var loginBtn = FBSDKLoginButton()
    let width = UIScreen.main.bounds.size.width
    let height = UIScreen.main.bounds.size.height
    let email_user = UILabel()
    var id_user = UILabel()
    var name_user = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("View did load")
        loginBtn.readPermissions = ["email"]
        let imageViewBackground = UIImageView(frame: CGRect(x:0,y: 0,width: width,height: height))
        imageViewBackground.image =  UIImage(named: "ap")
        imageViewBackground.contentMode = UIViewContentMode.scaleAspectFill
        self.view.addSubview(imageViewBackground)
        self.view.addSubview(loginBtn)
        loginBtn.center = view.center
        loginBtn.delegate = self
        
        if let token = FBSDKAccessToken.current(){
            print(token)
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email"]).start(completionHandler: { (connection, result, error) -> Void in
                let fbDetails = result as! NSDictionary
            
                print(fbDetails)
                for (key, value) in fbDetails{
                    print(key, value)
                    self.id_user.text = String(describing: value)
                }
        self.id_user.textColor = UIColor.green
        //self.id_user.font = UIFont(name:"HelveticaNeue-Bold", size: 16.0)
        self.id_user.frame = CGRect(x:10, y:20, width:100, height:100)
        self.view.addSubview(self.id_user)
                
        
        
        let email = Array(fbDetails)[0].value
        self.email_user.text = String(describing: email)
        self.email_user.textColor = UIColor.green
        //self.email_user.font = UIFont(name:"HelveticaNeue-Bold", size: 16.0)
        self.email_user.frame = CGRect(x:10, y:60, width:300, height:100)
        self.view.addSubview(self.email_user)

        
                    
                
            })
        }
        else{
            print("Token not correct")
        }
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        print("Completed Login")

        
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        id_user.isHidden = true
        email_user.isHidden =  true
        
    }
    func loginButtonWillLogin(_ loginButton: FBSDKLoginButton!) -> Bool {
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

